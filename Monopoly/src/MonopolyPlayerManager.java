
public class MonopolyPlayerManager {
	private static String name;
	private String piece;
	private static int pID;
	private static int position;
	private static int totalPlayers = 0;
	
	public MonopolyPlayerManager(String playerName, String playerSymbol, int currentPosition) {
		name = playerName;
		piece = playerSymbol;
		position = currentPosition;
		pID = totalPlayers;
		totalPlayers++;
	}
	
	public String getName() {
		return name;
	}
	
	public String getPiece() {
		return piece;
	}
	
	public int getPID() {
		return pID;
	}
	
	public int getPosition() {
		return position;
	}
	
	public static int getTotalPlayers() {
		return totalPlayers + 1;
	}
	
	public static String getPlayersOnSpace(int space) {
		String r = "";
		if(totalPlayers == 1 && MonopolyLaunch.p1.getPosition() == space)
			r += MonopolyLaunch.p1.getPiece();
		if(totalPlayers == 2 && MonopolyLaunch.p2.getPosition() == space)
			r += MonopolyLaunch.p2.getPiece();
		if(totalPlayers == 3 && MonopolyLaunch.p3.getPosition() == space)
			r += MonopolyLaunch.p3.getPiece();
		if(totalPlayers == 4 && MonopolyLaunch.p4.getPosition() == space)
			r += MonopolyLaunch.p4.getPiece();
		int x = 9 - r.length();
		for(int i = 1; i <= x; i++) { //Fills the rest of the space with spaces.
			r += " ";
		}
		/*
		 * Now, here is WHY to use i <= x rather than i <= 9 - r.length():
		 * As i increments, it rechecks r.length(). Since r will have a " "
		 * added to it, the length increases. When it gets to 5 spaces,
		 * 9 - 5 = 4, and at length 5 it has surpassed the new total of 4.
		 * 
		 * Huge thanks to edrington on the Tek Syndicate forums for pointing
		 * this out. For some reason, Eclipse will not debug this, so I would
		 * likely never have caught that otherwise.
		 * https://forum.teksyndicate.com/t/weird-bugs-in-eclipse-and-java-logic/95002/4?u=flamingglobe
		 */
		return r;
	}
	
	public int updatePosition(int change, boolean absolute) {
		if(absolute && change >= 0 && change < 40) { //Change directly to a specified property, do not advance, i.e. go directly to jail.
			position = change;
		} else {
			if(position + change >= 40) {
				position = Math.abs(39 - (position + change)) - 1; //The -1 adjusts it back to start at 0 again to prevent skipping Go.
			}
		}
		return position;
	}
}
