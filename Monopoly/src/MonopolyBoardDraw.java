/* Monopoly Board Draw
 * Displays a Monopoly board in colored ASCII art to the console.
 *  Copyright (C) 2016 David Lucadou
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//import org.fusesource.jansi.*;
import print.color.ColoredPrinter;
import print.color.ColoredPrinter.*;
import print.color.Ansi.*;
//import print.color.ColoredPrinterWIN.*;
import print.color.ColoredPrinterWIN;
//import print.TerminalPrinter;

public class MonopolyBoardDraw {
	//public static ColoredPrinter cPrinter;
	public static ColoredPrinterWIN cPrinter;
	
	public static void drawBoard() {
		//Line 1.
		cPrinter.println("╔═════════╦═════════╤═════════╤═════════╤═════════╤═════════╤═════════╤═════════╤═════════╤═════════╦═════════╗", Attribute.NONE, FColor.WHITE, BColor.NONE);
		cPrinter.print("║         ║", Attribute.NONE, FColor.WHITE, BColor.NONE); //Free parking.
		cPrinter.print("█████████", Attribute.NONE, FColor.RED, BColor.RED); //Canterlot gardens.
		cPrinter.print("│         │", Attribute.NONE, FColor.WHITE, BColor.NONE); //Chance.
		cPrinter.print("█████████", Attribute.NONE, FColor.RED, BColor.RED); //Ghastly Gorge.
		cPrinter.print("│", Attribute.NONE, FColor.WHITE, BColor.NONE); //Divider.
		cPrinter.print("█████████", Attribute.NONE, FColor.RED, BColor.RED); //Whitetail Woods.
		cPrinter.print("│         │", Attribute.NONE, FColor.WHITE, BColor.NONE); //Dodge Junction Station.
		cPrinter.print("█████████", Attribute.NONE, FColor.YELLOW, BColor.YELLOW); //The Everfree Forest.
		cPrinter.print("│", Attribute.NONE, FColor.WHITE, BColor.NONE); //Divider.
		cPrinter.print("█████████", Attribute.NONE, FColor.YELLOW, BColor.YELLOW); //Frogy Bottom Bog.
		cPrinter.print("│         │", Attribute.NONE, FColor.WHITE, BColor.NONE); //Weather Factory.
		cPrinter.print("█████████", Attribute.NONE, FColor.YELLOW, BColor.YELLOW); //Zecora's Hut.
		cPrinter.print("║" + MonopolyCardsDB.getJailData(1, 0)
				+ "║", Attribute.NONE, FColor.WHITE, BColor.NONE); //Go to Jail/Banished to the Moon.
		
		//Line 2.
		cPrinter.print("\n║  Free   ║", Attribute.NONE, FColor.WHITE, BColor.NONE); //Free Parking.
		cPrinter.print(MonopolyCardsDB.getCardData(14, 12)
				+ "│", Attribute.NONE, FColor.WHITE, BColor.NONE); //Canterlot Gardens.
		cPrinter.print(" Chance  │", Attribute.NONE, FColor.WHITE, BColor.NONE); //Chance.
		cPrinter.print(MonopolyCardsDB.getCardData(15, 12)
				+ "│", Attribute.NONE, FColor.WHITE, BColor.NONE); //Ghastly Gorge.
		cPrinter.print(MonopolyCardsDB.getCardData(16, 12)
				+ "│", Attribute.NONE, FColor.WHITE, BColor.NONE); //Whiletail Woods.
		cPrinter.print(MonopolyCardsDB.getCardData(17, 12)
				+ "│", Attribute.NONE, FColor.WHITE, BColor.NONE); //Dodge Junction Station.
		cPrinter.print(MonopolyCardsDB.getCardData(18, 12)
				+ "│", Attribute.NONE, FColor.WHITE, BColor.NONE); //The Everfree Forest.
		cPrinter.print(MonopolyCardsDB.getCardData(19, 12)
				+ "│", Attribute.NONE, FColor.WHITE, BColor.NONE); //Froggy Bottom Bog.
		cPrinter.print(MonopolyCardsDB.getCardData(20, 12)
				+ "│", Attribute.NONE, FColor.WHITE, BColor.NONE); //Weather Factory.
		cPrinter.print(MonopolyCardsDB.getCardData(21, 12)
				+ "║", Attribute.NONE, FColor.WHITE, BColor.NONE); //Zecora's Hut.
		cPrinter.print(MonopolyCardsDB.getJailData(1, 1)
				+ "║", Attribute.NONE, FColor.WHITE, BColor.NONE); //Go to Jail/Banished to the Moon.
		cPrinter.print("\n║ Parking ║", Attribute.NONE, FColor.WHITE, BColor.NONE); //Free Parking.
		cPrinter.print("         │", Attribute.NONE, FColor.WHITE, BColor.NONE); //Canterlot Gardens.
		cPrinter.print("         │", Attribute.NONE, FColor.WHITE, BColor.NONE); //Chance.
		cPrinter.print("         │", Attribute.NONE, FColor.WHITE, BColor.NONE); //Ghastly Gorge.
		cPrinter.print("         │", Attribute.NONE, FColor.WHITE, BColor.NONE); //Whitetail Woods.
		cPrinter.print("         │", Attribute.NONE, FColor.WHITE, BColor.NONE); //Dodge Junction Station.
		cPrinter.print("         │", Attribute.NONE, FColor.WHITE, BColor.NONE); //The Everfree Forest.
		cPrinter.print("         │", Attribute.NONE, FColor.WHITE, BColor.NONE); //Foggy Bottom Bog.
		cPrinter.print("         │", Attribute.NONE, FColor.WHITE, BColor.NONE); //Weather Factory.
		cPrinter.print("         ║", Attribute.NONE, FColor.WHITE, BColor.NONE); //Zecora's Hut.`
		cPrinter.print(MonopolyCardsDB.getJailData(1, 2)
				+ "║", Attribute.NONE, FColor.WHITE, BColor.NONE); //Go to Jail/Banished to the Moon.
				//Need a method to display Go to Jail or Go to Moon based on which deck is chosen.
		
		//Line 3.
		cPrinter.print("\n║", Attribute.NONE, FColor.WHITE, BColor.NONE);
		cPrinter.print("         ", Attribute.UNDERLINE, FColor.WHITE, BColor.NONE);
		cPrinter.print("║", Attribute.NONE, FColor.WHITE, BColor.NONE); //Free Parking.
		cPrinter.print(MonopolyCardsDB.getCardData(14, 13), Attribute.NONE, FColor.WHITE, BColor.NONE); //Canterlot Gardens.
		cPrinter.print("│", Attribute.NONE, FColor.WHITE, BColor.NONE);
		cPrinter.print("         ", Attribute.UNDERLINE, FColor.WHITE, BColor.NONE); //Chance.
		cPrinter.print("│", Attribute.NONE, FColor.WHITE, BColor.NONE);
		cPrinter.print(MonopolyCardsDB.getCardData(15, 13), Attribute.UNDERLINE, FColor.WHITE, BColor.NONE); //Ghastly Gorge.
		cPrinter.print("│", Attribute.NONE, FColor.WHITE, BColor.NONE);
		cPrinter.print(MonopolyCardsDB.getCardData(16, 13), Attribute.UNDERLINE, FColor.WHITE, BColor.NONE); //Whitetail Woods.
		cPrinter.print("│", Attribute.NONE, FColor.WHITE, BColor.NONE);
		cPrinter.print(MonopolyCardsDB.getCardData(17, 13), Attribute.UNDERLINE, FColor.WHITE, BColor.NONE); //Dodge Junction Station.
		cPrinter.print("│", Attribute.NONE, FColor.WHITE, BColor.NONE);
		cPrinter.print(MonopolyCardsDB.getCardData(18, 13), Attribute.UNDERLINE, FColor.WHITE, BColor.NONE); //The Everfree Forest.
		cPrinter.print("│", Attribute.NONE, FColor.WHITE, BColor.NONE);
		cPrinter.print(MonopolyCardsDB.getCardData(19, 13), Attribute.UNDERLINE, FColor.WHITE, BColor.NONE); //Froggy Bottom Bog.
		cPrinter.print("│", Attribute.NONE, FColor.WHITE, BColor.NONE);
		cPrinter.print(MonopolyCardsDB.getCardData(20, 13), Attribute.UNDERLINE, FColor.WHITE, BColor.NONE); //Weather Factory.
		cPrinter.print("│", Attribute.NONE, FColor.WHITE, BColor.NONE);
		cPrinter.print(MonopolyCardsDB.getCardData(21, 13), Attribute.UNDERLINE, FColor.WHITE, BColor.NONE); //Zecora's Hut.
		cPrinter.print("║", Attribute.NONE, FColor.WHITE, BColor.NONE);
		cPrinter.print("         ", Attribute.UNDERLINE, FColor.WHITE, BColor.NONE);
		cPrinter.print("║", Attribute.NONE, FColor.WHITE, BColor.NONE);
		
		//Line 4.
		cPrinter.print("\n║"
				+ MonopolyPlayerManager.getPlayersOnSpace(20)
				+ "║", Attribute.NONE, FColor.WHITE, BColor.NONE); //Free Parking.
		cPrinter.print(MonopolyPlayerManager.getPlayersOnSpace(21)
				+ "│", Attribute.NONE, FColor.WHITE, BColor.NONE); //Canterlot Gardens.
		cPrinter.print(MonopolyPlayerManager.getPlayersOnSpace(22)
				+ "│", Attribute.NONE, FColor.WHITE, BColor.NONE); //Chance.
		cPrinter.print(MonopolyPlayerManager.getPlayersOnSpace(23)
				+ "│", Attribute.NONE, FColor.WHITE, BColor.NONE); //Ghastly Gorge.
		cPrinter.print(MonopolyPlayerManager.getPlayersOnSpace(24)
				+ "│", Attribute.NONE, FColor.WHITE, BColor.NONE); //Whitetail Woods.
		cPrinter.print(MonopolyPlayerManager.getPlayersOnSpace(25)
				+ "│", Attribute.NONE, FColor.WHITE, BColor.NONE); //Dodge Junction Station.
		cPrinter.print(MonopolyPlayerManager.getPlayersOnSpace(26)
				+ "│", Attribute.NONE, FColor.WHITE, BColor.NONE); //The Everfree Forest.
		cPrinter.print(MonopolyPlayerManager.getPlayersOnSpace(27)
				+ "│", Attribute.NONE, FColor.WHITE, BColor.NONE); //Froggy Bottom Bog.
		cPrinter.print(MonopolyPlayerManager.getPlayersOnSpace(28)
				+ "│", Attribute.NONE, FColor.WHITE, BColor.NONE); //Weather Factory.
		cPrinter.print(MonopolyPlayerManager.getPlayersOnSpace(29)
				+ "║", Attribute.NONE, FColor.WHITE, BColor.NONE); //Zecora's Hut.
		cPrinter.print(MonopolyPlayerManager.getPlayersOnSpace(30)
				+ "║", Attribute.NONE, FColor.WHITE, BColor.NONE); //Banished to the Moon/Jail.
		
		//Line 5.
		cPrinter.print("\n╠═════════╬═════════╧═════════╧═════════╧═════════╧═════════╧═════════╧═════════╧═════════╧═════════╬═════════╣", Attribute.NONE, FColor.WHITE, BColor.NONE); //Free Parking.
		
		//Line 6.
		cPrinter.print("█████████", Attribute.NONE, FColor.RED, BColor.RED);
		
		
		
		
		
		System.out.println("test");
		
		
		
		
		
		//│		  │		 	│		  │		 	│		  │		 	│		  │		 	║		  ║", Attribute.NONE, FColor.WHITE, BColor.NONE);
	}
	
	public static void startColoredPrinter() {
		getPrinter();
		
		
		/*print.TerminalPrinter.Builder tBuilder = new TerminalPrinter.Builder(0, false);
		TerminalPrinter tPrinter = new TerminalPrinter(tBuilder);
		tPrinter.println("test");
		
		//ColoredPrinter cP = new ColoredPrinter.Builder(1, true);
		
		Builder cpBuilder = new ColoredPrinterNIX.Builder(1, true);
		ColoredPrinterNIX cPrinter = new ColoredPrinterNIX(cpBuilder);
		cPrinter.println("test", Attribute.BOLD, FColor.CYAN, BColor.MAGENTA); //lookup javadocs for type object, that might fix this.
		//cPrinter.debugPrint("test");											//also, just try to find examples of usage in general.
		cPrinter.debugPrintln("test 2", 1);*/
	}
	
	private static void getPrinter() {
		String os = System.getProperty("os.name");
	    //System.out.println("DETECTED OS: " + os);

	    if (os.toLowerCase().startsWith("win")) {
	        //cPrinter = new ColoredPrinterWIN.Builder(1, false).build();
	    } else {
	        cPrinter = new ColoredPrinterWIN.Builder(1, false).build();
	    }
	}
}



/*
 * ╔═════════╦═════════╤═════════╤═════════╤═════════╤═════════╤═════════╤═════════╤═════════╤═════════╤═════════╦═════════╗
 * ║		 ║COLOR/H&H│		 │		   │		 │		   │		 │		   │		 │		   │		 ║BANISHED-║
 * ║  Free	 ║ Property│  Chance │		   │		 │		   │		 │		   │		 │		   │		 ║Go to the║
 * ║ Parking ║ 		   │		 │		   │		 │		   │		 │		   │		 │		   │		 ║  Moon!  ║
 * ║ 		 ║  Price  │		 │		   │		 │		   │		 │		   │		 │		   │		 ║		   ║
 * ║		 ║[Players]│		 │		   │		 │		   │		 │		   │		 │		   │		 ║		   ║
 * ╠═════════╬═════════╧═════════╧═════════╧═════════╧═════════╧═════════╧═════════╧═════════╧═════════╧═════════╬═════════╣
 * = 
 * = 
 * = 
 * = 
 * = 
 */