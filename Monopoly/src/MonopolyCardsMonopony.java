/* Monopoly Cards - Monopony Set
 * Monopony card set for Monopoly.
 *  Copyright (C) 2016 David Lucadou
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class MonopolyCardsMonopony {
	public static void setCurrency() {
		MonopolyCardsDB.currency = "Bits";
	}
	
	public static void setJailText() { //[0] = Jail cell, [1] = Go to Jail.
		MonopolyCardsDB.jailText[0][0] = "Banishmnt";
		MonopolyCardsDB.jailText[0][1] = " The Moon"; //Underline this. If players are here, have them overwrite text, i.e. "X, Y     ".
		MonopolyCardsDB.jailText[0][2] = "   Just  ";
		MonopolyCardsDB.jailText[0][3] = "Observing";
		MonopolyCardsDB.jailText[0][4] = ""; //Need to get players.
		
		MonopolyCardsDB.jailText[1][0] = "Banished-";
		MonopolyCardsDB.jailText[1][1] = "Go to the";
		MonopolyCardsDB.jailText[1][2] = "  Moon!  ";
		MonopolyCardsDB.jailText[1][3] = "         ";
		MonopolyCardsDB.jailText[1][4] = "         "; //Maybe have a function that gets players, even though they will be moved down to Jail almost immediately.
	}
	
	public static void populateMonoponyDeck() {
		MonopolyCardsDB.activeDeck[0][0] = "Rock Farm"; //Dark purple #1.
		MonopolyCardsDB.activeDeck[0][1] = "Property"; //Property, railroad, or utility.
		MonopolyCardsDB.activeDeck[0][2] = Integer.toString(60); //Price.
		MonopolyCardsDB.activeDeck[0][3] = Integer.toString(2); //Rent.
		MonopolyCardsDB.activeDeck[0][4] = Integer.toString(10); //Rent with 1 house.
		MonopolyCardsDB.activeDeck[0][5] = Integer.toString(30); //Rent with 2 houses.
		MonopolyCardsDB.activeDeck[0][6] = Integer.toString(90); //Rent with 3 houses.
		MonopolyCardsDB.activeDeck[0][7] = Integer.toString(160); //Rent with 4 houses.
		MonopolyCardsDB.activeDeck[0][8] = Integer.toString(250); //Rent with hotel.
		MonopolyCardsDB.activeDeck[0][9] = Integer.toString(30); //Mortgage value.
		MonopolyCardsDB.activeDeck[0][10] = Integer.toString(50); //Price of houses.
		MonopolyCardsDB.activeDeck[0][11] = Integer.toString(50); //Price of hotels.
		MonopolyCardsDB.activeDeck[0][12] = "Rock Farm"; //Abbreviated name.
		MonopolyCardsDB.activeDeck[0][13] = "   60    "; //Price for display purposes.
		
		MonopolyCardsDB.activeDeck[1][0] = "Ponyville"; //Dark purple #2.
		MonopolyCardsDB.activeDeck[1][1] = "Property"; //Property, railroad, or utility.
		MonopolyCardsDB.activeDeck[1][2] = Integer.toString(80); //Price.
		MonopolyCardsDB.activeDeck[1][3] = Integer.toString(4); //Rent.
		MonopolyCardsDB.activeDeck[1][4] = Integer.toString(20); //Rent with 1 house.
		MonopolyCardsDB.activeDeck[1][5] = Integer.toString(60); //Rent with 2 houses.
		MonopolyCardsDB.activeDeck[1][6] = Integer.toString(180); //Rent with 3 houses.
		MonopolyCardsDB.activeDeck[1][7] = Integer.toString(320); //Rent with 4 houses.
		MonopolyCardsDB.activeDeck[1][8] = Integer.toString(450); //Rent with hotel.
		MonopolyCardsDB.activeDeck[1][9] = Integer.toString(30); //Mortgage value.
		MonopolyCardsDB.activeDeck[1][10] = Integer.toString(50); //Price of houses.
		MonopolyCardsDB.activeDeck[1][11] = Integer.toString(50); //Price of hotels.
		MonopolyCardsDB.activeDeck[1][12] = "Ponyville"; //Abbreviated name.
		MonopolyCardsDB.activeDeck[1][13] = "   80    "; //Price for display purposes.
		
		MonopolyCardsDB.activeDeck[2][0] = "Ponyville Station"; //Railroad #1.
		MonopolyCardsDB.activeDeck[2][1] = "Railroad"; //Property, railroad, or utility.
		MonopolyCardsDB.activeDeck[2][2] = Integer.toString(200); //Price.
		MonopolyCardsDB.activeDeck[2][3] = Integer.toString(25); //Rent with 1 railroad.
		MonopolyCardsDB.activeDeck[2][4] = Integer.toString(50); //Rent with 2 railroads.
		MonopolyCardsDB.activeDeck[2][5] = Integer.toString(100); //Rent with 3 railroads.
		MonopolyCardsDB.activeDeck[2][6] = Integer.toString(200); //Rent with 4 railroads.
		MonopolyCardsDB.activeDeck[2][7] = null; //Rent with 4 houses.
		MonopolyCardsDB.activeDeck[2][8] = null; //Rent with hotel.
		MonopolyCardsDB.activeDeck[2][9] = Integer.toString(100); //Mortgage value.
		MonopolyCardsDB.activeDeck[2][10] = null; //Price of houses.
		MonopolyCardsDB.activeDeck[2][11] = null; //Price of hotels.
		MonopolyCardsDB.activeDeck[2][12] = "Pnyv RR"; //Abbreviated name.
		MonopolyCardsDB.activeDeck[2][13] = "   200   "; //Price for display purposes.
		
		MonopolyCardsDB.activeDeck[3][0] = "Fillydelphia"; //Light blue #1.
		MonopolyCardsDB.activeDeck[3][1] = "Property"; //Property, railroad, or utility.
		MonopolyCardsDB.activeDeck[3][2] = Integer.toString(100); //Price.
		MonopolyCardsDB.activeDeck[3][3] = Integer.toString(6); //Rent.
		MonopolyCardsDB.activeDeck[3][4] = Integer.toString(30); //Rent with 1 house.
		MonopolyCardsDB.activeDeck[3][5] = Integer.toString(90); //Rent with 2 houses.
		MonopolyCardsDB.activeDeck[3][6] = Integer.toString(270); //Rent with 3 houses.
		MonopolyCardsDB.activeDeck[3][7] = Integer.toString(400); //Rent with 4 houses.
		MonopolyCardsDB.activeDeck[3][8] = Integer.toString(550); //Rent with hotel.
		MonopolyCardsDB.activeDeck[3][9] = Integer.toString(50); //Mortgage value.
		MonopolyCardsDB.activeDeck[3][10] = Integer.toString(50); //Price of houses.
		MonopolyCardsDB.activeDeck[3][11] = Integer.toString(50); //Price of hotels.
		MonopolyCardsDB.activeDeck[3][12] = "Fillydel."; //Abbreviated name.
		MonopolyCardsDB.activeDeck[3][13] = "   100   "; //Price for display purposes.
		
		MonopolyCardsDB.activeDeck[4][0] = "Hoofington"; //Light blue #2.
		MonopolyCardsDB.activeDeck[4][1] = "Property"; //Property, railroad, or utility.
		MonopolyCardsDB.activeDeck[4][2] = Integer.toString(100); //Price.
		MonopolyCardsDB.activeDeck[4][3] = Integer.toString(6); //Rent.
		MonopolyCardsDB.activeDeck[4][4] = Integer.toString(30); //Rent with 1 house.
		MonopolyCardsDB.activeDeck[4][5] = Integer.toString(90); //Rent with 2 houses.
		MonopolyCardsDB.activeDeck[4][6] = Integer.toString(270); //Rent with 3 houses.
		MonopolyCardsDB.activeDeck[4][7] = Integer.toString(400); //Rent with 4 houses.
		MonopolyCardsDB.activeDeck[4][8] = Integer.toString(550); //Rent with hotel.
		MonopolyCardsDB.activeDeck[4][9] = Integer.toString(50); //Mortgage value.
		MonopolyCardsDB.activeDeck[4][10] = Integer.toString(50); //Price of houses.
		MonopolyCardsDB.activeDeck[4][11] = Integer.toString(50); //Price of hotels.
		MonopolyCardsDB.activeDeck[4][12] = "Hoofingt."; //Abbreviated name.
		MonopolyCardsDB.activeDeck[4][13] = "   100   "; //Price for display purposes.
		
		MonopolyCardsDB.activeDeck[5][0] = "Trottingham"; //Light blue #3.
		MonopolyCardsDB.activeDeck[5][1] = "Property"; //Property, railroad, or utility.
		MonopolyCardsDB.activeDeck[5][2] = Integer.toString(120); //Price.
		MonopolyCardsDB.activeDeck[5][3] = Integer.toString(8); //Rent.
		MonopolyCardsDB.activeDeck[5][4] = Integer.toString(40); //Rent with 1 house.
		MonopolyCardsDB.activeDeck[5][5] = Integer.toString(100); //Rent with 2 houses.
		MonopolyCardsDB.activeDeck[5][6] = Integer.toString(300); //Rent with 3 houses.
		MonopolyCardsDB.activeDeck[5][7] = Integer.toString(450); //Rent with 4 houses.
		MonopolyCardsDB.activeDeck[5][8] = Integer.toString(600); //Rent with hotel.
		MonopolyCardsDB.activeDeck[5][9] = Integer.toString(60); //Mortgage value.
		MonopolyCardsDB.activeDeck[5][10] = Integer.toString(50); //Price of houses.
		MonopolyCardsDB.activeDeck[5][11] = Integer.toString(50); //Price of hotels.
		MonopolyCardsDB.activeDeck[5][12] = "Trtngham."; //Abbreviated name.
		MonopolyCardsDB.activeDeck[5][13] = "   120   "; //Price for display purposes.
		
		MonopolyCardsDB.activeDeck[6][0] = "Las Pegasus"; //Light purple/magenta #1.
		MonopolyCardsDB.activeDeck[6][1] = "Property"; //Property, railroad, or utility.
		MonopolyCardsDB.activeDeck[6][2] = Integer.toString(140); //Price.
		MonopolyCardsDB.activeDeck[6][3] = Integer.toString(10); //Rent.
		MonopolyCardsDB.activeDeck[6][4] = Integer.toString(50); //Rent with 1 house.
		MonopolyCardsDB.activeDeck[6][5] = Integer.toString(150); //Rent with 2 houses.
		MonopolyCardsDB.activeDeck[6][6] = Integer.toString(450); //Rent with 3 houses.
		MonopolyCardsDB.activeDeck[6][7] = Integer.toString(625); //Rent with 4 houses.
		MonopolyCardsDB.activeDeck[6][8] = Integer.toString(750); //Rent with hotel.
		MonopolyCardsDB.activeDeck[6][9] = Integer.toString(70); //Mortgage value.
		MonopolyCardsDB.activeDeck[6][10] = Integer.toString(100); //Price of houses.
		MonopolyCardsDB.activeDeck[6][11] = Integer.toString(100); //Price of hotels.
		MonopolyCardsDB.activeDeck[6][12] = "Ls Pgsus."; //Abbreviated name.
		MonopolyCardsDB.activeDeck[6][13] = "   140   "; //Price for display purposes.
		
		MonopolyCardsDB.activeDeck[7][0] = "Apple Harvest"; //Utility #1.
		MonopolyCardsDB.activeDeck[7][1] = "Utility"; //Property, railroad, or utility.
		MonopolyCardsDB.activeDeck[7][2] = Integer.toString(150); //Price.
		MonopolyCardsDB.activeDeck[7][3] = Integer.toString(4); //Rent multiplier for one utility.
		MonopolyCardsDB.activeDeck[7][4] = Integer.toString(10); //Rent multiplier for two utilities.
		MonopolyCardsDB.activeDeck[7][5] = null; //No houses.
		MonopolyCardsDB.activeDeck[7][6] = null; //No houses.
		MonopolyCardsDB.activeDeck[7][7] = null; //No houses.
		MonopolyCardsDB.activeDeck[7][8] = null; //No hotels.
		MonopolyCardsDB.activeDeck[7][9] = Integer.toString(75); //Mortgage value.
		MonopolyCardsDB.activeDeck[7][10] = null; //No houses.
		MonopolyCardsDB.activeDeck[7][11] = null; //No hotels.
		MonopolyCardsDB.activeDeck[7][12] = "Apl Hvst."; //Abbreviated name.
		MonopolyCardsDB.activeDeck[7][13] = "   150   "; //Price for display purposes.
		
		MonopolyCardsDB.activeDeck[8][0] = "Baltimare"; //Light purple/magenta #2.
		MonopolyCardsDB.activeDeck[8][1] = "Property"; //Property, railroad, or utility.
		MonopolyCardsDB.activeDeck[8][2] = Integer.toString(140); //Price.
		MonopolyCardsDB.activeDeck[8][3] = Integer.toString(10); //Rent.
		MonopolyCardsDB.activeDeck[8][4] = Integer.toString(50); //Rent with 1 house.
		MonopolyCardsDB.activeDeck[8][5] = Integer.toString(150); //Rent with 2 houses.
		MonopolyCardsDB.activeDeck[8][6] = Integer.toString(450); //Rent with 3 houses.
		MonopolyCardsDB.activeDeck[8][7] = Integer.toString(625); //Rent with 4 houses.
		MonopolyCardsDB.activeDeck[8][8] = Integer.toString(750); //Rent with hotel.
		MonopolyCardsDB.activeDeck[8][9] = Integer.toString(70); //Mortgage value.
		MonopolyCardsDB.activeDeck[8][10] = Integer.toString(100); //Price of houses.
		MonopolyCardsDB.activeDeck[8][11] = Integer.toString(100); //Price of hotels.
		MonopolyCardsDB.activeDeck[8][12] = "Baltimare"; //(Un-)abbreviated name.
		MonopolyCardsDB.activeDeck[8][13] = "   140   "; //Price for display purposes.
		
		MonopolyCardsDB.activeDeck[9][0] = "Manehattan"; //Light purple/magenta #3.
		MonopolyCardsDB.activeDeck[9][1] = "Property"; //Property, railroad, or utility.
		MonopolyCardsDB.activeDeck[9][2] = Integer.toString(160); //Price.
		MonopolyCardsDB.activeDeck[9][3] = Integer.toString(12); //Rent.
		MonopolyCardsDB.activeDeck[9][4] = Integer.toString(60); //Rent with 1 house.
		MonopolyCardsDB.activeDeck[9][5] = Integer.toString(180); //Rent with 2 houses.
		MonopolyCardsDB.activeDeck[9][6] = Integer.toString(500); //Rent with 3 houses.
		MonopolyCardsDB.activeDeck[9][7] = Integer.toString(700); //Rent with 4 houses.
		MonopolyCardsDB.activeDeck[9][8] = Integer.toString(900); //Rent with hotel.
		MonopolyCardsDB.activeDeck[9][9] = Integer.toString(80); //Mortgage value.
		MonopolyCardsDB.activeDeck[9][10] = Integer.toString(100); //Price of houses.
		MonopolyCardsDB.activeDeck[9][11] = Integer.toString(100); //Price of hotels.
		MonopolyCardsDB.activeDeck[9][12] = "Manehttn."; //Abbreviated name.
		MonopolyCardsDB.activeDeck[9][13] = "   160   "; //Price for display purposes.
		
		MonopolyCardsDB.activeDeck[10][0] = "Appleoosa Station"; //Railroad #2.
		MonopolyCardsDB.activeDeck[10][1] = "Railroad"; //Property, railroad, or utility.
		MonopolyCardsDB.activeDeck[10][2] = Integer.toString(200); //Price.
		MonopolyCardsDB.activeDeck[10][3] = Integer.toString(25); //Rent with 1 railroad.
		MonopolyCardsDB.activeDeck[10][4] = Integer.toString(50); //Rent with 2 railroads.
		MonopolyCardsDB.activeDeck[10][5] = Integer.toString(100); //Rent with 3 railroads.
		MonopolyCardsDB.activeDeck[10][6] = Integer.toString(200); //Rent with 4 railroads.
		MonopolyCardsDB.activeDeck[10][7] = null; //No houses.
		MonopolyCardsDB.activeDeck[10][8] = null; //No hotels.
		MonopolyCardsDB.activeDeck[10][9] = Integer.toString(100); //Mortgage value.
		MonopolyCardsDB.activeDeck[10][10] = null; //No houses.
		MonopolyCardsDB.activeDeck[10][11] = null; //No hotels.
		MonopolyCardsDB.activeDeck[10][12] = "Aplsa RR."; //Abbreviated name.
		MonopolyCardsDB.activeDeck[10][13] = "   200   "; //Price for display purposes.
		
		MonopolyCardsDB.activeDeck[11][0] = "Appleoosa"; //Orange #1.
		MonopolyCardsDB.activeDeck[11][1] = "Property"; //Property, railroad, or utility.
		MonopolyCardsDB.activeDeck[11][2] = Integer.toString(180); //Price.
		MonopolyCardsDB.activeDeck[11][3] = Integer.toString(14); //Rent.
		MonopolyCardsDB.activeDeck[11][4] = Integer.toString(70); //Rent with 1 house.
		MonopolyCardsDB.activeDeck[11][5] = Integer.toString(200); //Rent with 2 houses.
		MonopolyCardsDB.activeDeck[11][6] = Integer.toString(550); //Rent with 3 houses.
		MonopolyCardsDB.activeDeck[11][7] = Integer.toString(750); //Rent with 4 houses.
		MonopolyCardsDB.activeDeck[11][8] = Integer.toString(950); //Rent with hotel.
		MonopolyCardsDB.activeDeck[11][9] = Integer.toString(90); //Mortgage value.
		MonopolyCardsDB.activeDeck[11][10] = Integer.toString(100); //Price of houses.
		MonopolyCardsDB.activeDeck[11][11] = Integer.toString(100); //Price of hotels.
		MonopolyCardsDB.activeDeck[11][12] = "Appleoosa"; //(Un-)abbreviated name.
		MonopolyCardsDB.activeDeck[11][13] = "   180   "; //Price for display purposes.
		
		MonopolyCardsDB.activeDeck[12][0] = "Dodge Junction"; //Orange #2.
		MonopolyCardsDB.activeDeck[12][1] = "Property"; //Property, railroad, or utility.
		MonopolyCardsDB.activeDeck[12][2] = Integer.toString(180); //Price.
		MonopolyCardsDB.activeDeck[12][3] = Integer.toString(14); //Rent.
		MonopolyCardsDB.activeDeck[12][4] = Integer.toString(70); //Rent with 1 house.
		MonopolyCardsDB.activeDeck[12][5] = Integer.toString(200); //Rent with 2 houses.
		MonopolyCardsDB.activeDeck[12][6] = Integer.toString(550); //Rent with 3 houses.
		MonopolyCardsDB.activeDeck[12][7] = Integer.toString(750); //Rent with 4 houses.
		MonopolyCardsDB.activeDeck[12][8] = Integer.toString(950); //Rent with hotel.
		MonopolyCardsDB.activeDeck[12][9] = Integer.toString(90); //Mortgage value.
		MonopolyCardsDB.activeDeck[12][10] = Integer.toString(100); //Price of houses.
		MonopolyCardsDB.activeDeck[12][11] = Integer.toString(100); //Price of hotels.
		MonopolyCardsDB.activeDeck[12][12] = "Dgde Jct."; //Abbreviated name.
		MonopolyCardsDB.activeDeck[12][13] = "   180   "; //Price for display purposes.
		
		MonopolyCardsDB.activeDeck[13][0] = "Sweet Apple Acres"; //Orange #3.
		MonopolyCardsDB.activeDeck[13][1] = "Property"; //Property, railroad, or utility.
		MonopolyCardsDB.activeDeck[13][2] = Integer.toString(200); //Price.
		MonopolyCardsDB.activeDeck[13][3] = Integer.toString(16); //Rent.
		MonopolyCardsDB.activeDeck[13][4] = Integer.toString(80); //Rent with 1 house.
		MonopolyCardsDB.activeDeck[13][5] = Integer.toString(220); //Rent with 2 houses.
		MonopolyCardsDB.activeDeck[13][6] = Integer.toString(600); //Rent with 3 houses.
		MonopolyCardsDB.activeDeck[13][7] = Integer.toString(800); //Rent with 4 houses.
		MonopolyCardsDB.activeDeck[13][8] = Integer.toString(1000); //Rent with hotel.
		MonopolyCardsDB.activeDeck[13][9] = Integer.toString(100); //Mortgage value.
		MonopolyCardsDB.activeDeck[13][10] = Integer.toString(100); //Price of houses.
		MonopolyCardsDB.activeDeck[13][11] = Integer.toString(100); //Price of hotels.
		MonopolyCardsDB.activeDeck[13][12] = "Sw Apl A."; //Abbreviated name.
		MonopolyCardsDB.activeDeck[13][13] = "   200   "; //Price for display purposes.
		
		MonopolyCardsDB.activeDeck[14][0] = "Canterlot Gardens"; //Red #1.
		MonopolyCardsDB.activeDeck[14][1] = "Property"; //Property, railroad, or utility.
		MonopolyCardsDB.activeDeck[14][2] = Integer.toString(220); //Price.
		MonopolyCardsDB.activeDeck[14][3] = Integer.toString(18); //Rent.
		MonopolyCardsDB.activeDeck[14][4] = Integer.toString(90); //Rent with 1 house.
		MonopolyCardsDB.activeDeck[14][5] = Integer.toString(250); //Rent with 2 houses.
		MonopolyCardsDB.activeDeck[14][6] = Integer.toString(700); //Rent with 3 houses.
		MonopolyCardsDB.activeDeck[14][7] = Integer.toString(875); //Rent with 4 houses.
		MonopolyCardsDB.activeDeck[14][8] = Integer.toString(1050); //Rent with hotel.
		MonopolyCardsDB.activeDeck[14][9] = Integer.toString(110); //Mortgage value.
		MonopolyCardsDB.activeDeck[14][10] = Integer.toString(150); //Price of houses.
		MonopolyCardsDB.activeDeck[14][11] = Integer.toString(150); //Price of hotels.
		MonopolyCardsDB.activeDeck[14][12] = "Cl Grdns."; //Abbreviated name.
		MonopolyCardsDB.activeDeck[14][13] = "   220   "; //Price for display purposes.
		
		MonopolyCardsDB.activeDeck[15][0] = "Ghastly Gorge"; //Red #2.
		MonopolyCardsDB.activeDeck[15][1] = "Property"; //Property, railroad, or utility.
		MonopolyCardsDB.activeDeck[15][2] = Integer.toString(220); //Price.
		MonopolyCardsDB.activeDeck[15][3] = Integer.toString(18); //Rent.
		MonopolyCardsDB.activeDeck[15][4] = Integer.toString(90); //Rent with 1 house.
		MonopolyCardsDB.activeDeck[15][5] = Integer.toString(250); //Rent with 2 houses.
		MonopolyCardsDB.activeDeck[15][6] = Integer.toString(700); //Rent with 3 houses.
		MonopolyCardsDB.activeDeck[15][7] = Integer.toString(875); //Rent with 4 houses.
		MonopolyCardsDB.activeDeck[15][8] = Integer.toString(1050); //Rent with hotel.
		MonopolyCardsDB.activeDeck[15][9] = Integer.toString(110); //Mortgage value.
		MonopolyCardsDB.activeDeck[15][10] = Integer.toString(150); //Price of houses.
		MonopolyCardsDB.activeDeck[15][11] = Integer.toString(150); //Price of hotels.
		MonopolyCardsDB.activeDeck[15][12] = "Gstl Grg."; //Abbreviated name.
		MonopolyCardsDB.activeDeck[15][13] = "   220   "; //Price for display purposes.
		
		MonopolyCardsDB.activeDeck[16][0] = "Whitetail Woods"; //Red #3.
		MonopolyCardsDB.activeDeck[16][1] = "Property"; //Property, railroad, or utility.
		MonopolyCardsDB.activeDeck[16][2] = Integer.toString(240); //Price.
		MonopolyCardsDB.activeDeck[16][3] = Integer.toString(20); //Rent.
		MonopolyCardsDB.activeDeck[16][4] = Integer.toString(100); //Rent with 1 house.
		MonopolyCardsDB.activeDeck[16][5] = Integer.toString(300); //Rent with 2 houses.
		MonopolyCardsDB.activeDeck[16][6] = Integer.toString(750); //Rent with 3 houses.
		MonopolyCardsDB.activeDeck[16][7] = Integer.toString(925); //Rent with 4 houses.
		MonopolyCardsDB.activeDeck[16][8] = Integer.toString(1100); //Rent with hotel.
		MonopolyCardsDB.activeDeck[16][9] = Integer.toString(120); //Mortgage value.
		MonopolyCardsDB.activeDeck[16][10] = Integer.toString(150); //Price of houses.
		MonopolyCardsDB.activeDeck[16][11] = Integer.toString(150); //Price of hotels.
		MonopolyCardsDB.activeDeck[16][12] = "Whtl Wds."; //Abbreviated name.
		MonopolyCardsDB.activeDeck[16][13] = "   240   "; //Price for display purposes.
		
		MonopolyCardsDB.activeDeck[17][0] = "Dodge Junction Station"; //Railroad #3.
		MonopolyCardsDB.activeDeck[17][1] = "Railroad"; //Property, railroad, or utility.
		MonopolyCardsDB.activeDeck[17][2] = Integer.toString(200); //Price.
		MonopolyCardsDB.activeDeck[17][3] = Integer.toString(25); //Rent with 1 railroad.
		MonopolyCardsDB.activeDeck[17][4] = Integer.toString(50); //Rent with 2 railroads.
		MonopolyCardsDB.activeDeck[17][5] = Integer.toString(100); //Rent with 3 railroads.
		MonopolyCardsDB.activeDeck[17][6] = Integer.toString(200); //Rent with 4 railroads.
		MonopolyCardsDB.activeDeck[17][7] = null; //No houses.
		MonopolyCardsDB.activeDeck[17][8] = null; //No hotels.
		MonopolyCardsDB.activeDeck[17][9] = Integer.toString(100); //Mortgage value.
		MonopolyCardsDB.activeDeck[17][10] = null; //No houses.
		MonopolyCardsDB.activeDeck[17][11] = null; //No hotels.
		MonopolyCardsDB.activeDeck[17][12] = "Dg Jn RR."; //Abbreviated name.
		MonopolyCardsDB.activeDeck[17][13] = "   200   "; //Price for display purposes.
		
		MonopolyCardsDB.activeDeck[18][0] = "The Everfree Forest"; //Yellow #1.
		MonopolyCardsDB.activeDeck[18][1] = "Property"; //Property, railroad, or utility.
		MonopolyCardsDB.activeDeck[18][2] = Integer.toString(260); //Price.
		MonopolyCardsDB.activeDeck[18][3] = Integer.toString(22); //Rent.
		MonopolyCardsDB.activeDeck[18][4] = Integer.toString(110); //Rent with 1 house.
		MonopolyCardsDB.activeDeck[18][5] = Integer.toString(330); //Rent with 2 houses.
		MonopolyCardsDB.activeDeck[18][6] = Integer.toString(800); //Rent with 3 houses.
		MonopolyCardsDB.activeDeck[18][7] = Integer.toString(975); //Rent with 4 houses.
		MonopolyCardsDB.activeDeck[18][8] = Integer.toString(1150); //Rent with hotel.
		MonopolyCardsDB.activeDeck[18][9] = Integer.toString(130); //Mortgage value.
		MonopolyCardsDB.activeDeck[18][10] = Integer.toString(150); //Price of houses.
		MonopolyCardsDB.activeDeck[18][11] = Integer.toString(150); //Price of hotels.
		MonopolyCardsDB.activeDeck[18][12] = "Evf Frst."; //Abbreviated name.
		MonopolyCardsDB.activeDeck[18][13] = "   260   "; //Price for display purposes.
		
		MonopolyCardsDB.activeDeck[19][0] = "Froggy Bottom Bog"; //Yellow #2.
		MonopolyCardsDB.activeDeck[19][1] = "Property"; //Property, railroad, or utility.
		MonopolyCardsDB.activeDeck[19][2] = Integer.toString(260); //Price.
		MonopolyCardsDB.activeDeck[19][3] = Integer.toString(22); //Rent.
		MonopolyCardsDB.activeDeck[19][4] = Integer.toString(110); //Rent with 1 house.
		MonopolyCardsDB.activeDeck[19][5] = Integer.toString(330); //Rent with 2 houses.
		MonopolyCardsDB.activeDeck[19][6] = Integer.toString(800); //Rent with 3 houses.
		MonopolyCardsDB.activeDeck[19][7] = Integer.toString(975); //Rent with 4 houses.
		MonopolyCardsDB.activeDeck[19][8] = Integer.toString(1150); //Rent with hotel.
		MonopolyCardsDB.activeDeck[19][9] = Integer.toString(130); //Mortgage value.
		MonopolyCardsDB.activeDeck[19][10] = Integer.toString(150); //Price of houses.
		MonopolyCardsDB.activeDeck[19][11] = Integer.toString(150); //Price of hotels.
		MonopolyCardsDB.activeDeck[19][12] = "Frg B Bg."; //Abbreviated name.
		MonopolyCardsDB.activeDeck[19][13] = "   260   "; //Price for display purposes.
		
		MonopolyCardsDB.activeDeck[20][0] = "Weather Factory"; //Utility #2.
		MonopolyCardsDB.activeDeck[20][1] = "Utility"; //Property, railroad, or utility.
		MonopolyCardsDB.activeDeck[20][2] = Integer.toString(150); //Price.
		MonopolyCardsDB.activeDeck[20][3] = Integer.toString(4); //Rent multiplier, one utility.
		MonopolyCardsDB.activeDeck[20][4] = Integer.toString(10); //Rent multiplier, two utilities.
		MonopolyCardsDB.activeDeck[20][5] = null; //No houses.
		MonopolyCardsDB.activeDeck[20][6] = null; //No houses.
		MonopolyCardsDB.activeDeck[20][7] = null; //No houses.
		MonopolyCardsDB.activeDeck[20][8] = null; //No hotels.
		MonopolyCardsDB.activeDeck[20][9] = Integer.toString(75); //Mortgage value.
		MonopolyCardsDB.activeDeck[20][10] = null; //No houses.
		MonopolyCardsDB.activeDeck[20][11] = null; //No hotels.
		MonopolyCardsDB.activeDeck[20][12] = "Wthr Fct."; //Abbreviated name.
		MonopolyCardsDB.activeDeck[20][13] = "   150   "; //Price for display purposes.
		
		MonopolyCardsDB.activeDeck[21][0] = "Zecora's Hut"; //Yellow #3.
		MonopolyCardsDB.activeDeck[21][1] = "Property"; //Property, railroad, or utility.
		MonopolyCardsDB.activeDeck[21][2] = Integer.toString(280); //Price.
		MonopolyCardsDB.activeDeck[21][3] = Integer.toString(24); //Rent.
		MonopolyCardsDB.activeDeck[21][4] = Integer.toString(120); //Rent with 1 house.
		MonopolyCardsDB.activeDeck[21][5] = Integer.toString(360); //Rent with 2 houses.
		MonopolyCardsDB.activeDeck[21][6] = Integer.toString(850); //Rent with 3 houses.
		MonopolyCardsDB.activeDeck[21][7] = Integer.toString(1025); //Rent with 4 houses.
		MonopolyCardsDB.activeDeck[21][8] = Integer.toString(1200); //Rent with hotel.
		MonopolyCardsDB.activeDeck[21][9] = Integer.toString(140); //Mortgage value.
		MonopolyCardsDB.activeDeck[21][10] = Integer.toString(150); //Price of houses.
		MonopolyCardsDB.activeDeck[21][11] = Integer.toString(150); //Price of hotels.
		MonopolyCardsDB.activeDeck[21][12] = "Zcra. Hut"; //Abbreviated name.
		MonopolyCardsDB.activeDeck[21][13] = "   280   "; //Price for display purposes.
		
		MonopolyCardsDB.activeDeck[22][0] = "Sugarcube Corner"; //Green #1.
		MonopolyCardsDB.activeDeck[22][1] = "Property"; //Property, railroad, or utility.
		MonopolyCardsDB.activeDeck[22][2] = Integer.toString(300); //Price.
		MonopolyCardsDB.activeDeck[22][3] = Integer.toString(26); //Rent.
		MonopolyCardsDB.activeDeck[22][4] = Integer.toString(130); //Rent with 1 house.
		MonopolyCardsDB.activeDeck[22][5] = Integer.toString(390); //Rent with 2 houses.
		MonopolyCardsDB.activeDeck[22][6] = Integer.toString(900); //Rent with 3 houses.
		MonopolyCardsDB.activeDeck[22][7] = Integer.toString(1100); //Rent with 4 houses.
		MonopolyCardsDB.activeDeck[22][8] = Integer.toString(1275); //Rent with hotel.
		MonopolyCardsDB.activeDeck[22][9] = Integer.toString(150); //Mortgage value.
		MonopolyCardsDB.activeDeck[22][10] = Integer.toString(200); //Price of houses.
		MonopolyCardsDB.activeDeck[22][11] = Integer.toString(200); //Price of hotels.
		MonopolyCardsDB.activeDeck[22][12] = "Sgcb Cnr."; //Abbreviated name.
		MonopolyCardsDB.activeDeck[22][13] = "   300   "; //Price for display purposes.
		
		MonopolyCardsDB.activeDeck[23][0] = "Carousel Botique"; //Green #2.
		MonopolyCardsDB.activeDeck[23][1] = "Property"; //Property, railroad, or utility.
		MonopolyCardsDB.activeDeck[23][2] = Integer.toString(300); //Price.
		MonopolyCardsDB.activeDeck[23][3] = Integer.toString(26); //Rent.
		MonopolyCardsDB.activeDeck[23][4] = Integer.toString(130); //Rent with 1 house.
		MonopolyCardsDB.activeDeck[23][5] = Integer.toString(390); //Rent with 2 houses.
		MonopolyCardsDB.activeDeck[23][6] = Integer.toString(900); //Rent with 3 houses.
		MonopolyCardsDB.activeDeck[23][7] = Integer.toString(1100); //Rent with 4 houses.
		MonopolyCardsDB.activeDeck[23][8] = Integer.toString(1275); //Rent with hotel.
		MonopolyCardsDB.activeDeck[23][9] = Integer.toString(150); //Mortgage value.
		MonopolyCardsDB.activeDeck[23][10] = Integer.toString(200); //Price of houses.
		MonopolyCardsDB.activeDeck[23][11] = Integer.toString(200); //Price of hotels.
		MonopolyCardsDB.activeDeck[23][12] = "Crsl Btq."; //Abbreviated name.
		MonopolyCardsDB.activeDeck[23][13] = "   300   "; //Price for display purposes.
		
		MonopolyCardsDB.activeDeck[24][0] = "School House"; //Green #3.
		MonopolyCardsDB.activeDeck[24][1] = "Property"; //Property, railroad, or utility.
		MonopolyCardsDB.activeDeck[24][2] = Integer.toString(320); //Price.
		MonopolyCardsDB.activeDeck[24][3] = Integer.toString(28); //Rent.
		MonopolyCardsDB.activeDeck[24][4] = Integer.toString(150); //Rent with 1 house.
		MonopolyCardsDB.activeDeck[24][5] = Integer.toString(450); //Rent with 2 houses.
		MonopolyCardsDB.activeDeck[24][6] = Integer.toString(1000); //Rent with 3 houses.
		MonopolyCardsDB.activeDeck[24][7] = Integer.toString(1200); //Rent with 4 houses.
		MonopolyCardsDB.activeDeck[24][8] = Integer.toString(1400); //Rent with hotel.
		MonopolyCardsDB.activeDeck[24][9] = Integer.toString(160); //Mortgage value.
		MonopolyCardsDB.activeDeck[24][10] = Integer.toString(200); //Price of houses.
		MonopolyCardsDB.activeDeck[24][11] = Integer.toString(200); //Price of hotels.
		MonopolyCardsDB.activeDeck[25][12] = "Schl Hse."; //Abbreviated name.
		MonopolyCardsDB.activeDeck[26][13] = "   320   "; //Price for display purposes.
		
		MonopolyCardsDB.activeDeck[25][0] = "Canterlot Station"; //Railroad #4.
		MonopolyCardsDB.activeDeck[25][1] = "Railroad"; //Property, railroad, or utility.
		MonopolyCardsDB.activeDeck[25][2] = Integer.toString(200); //Price.
		MonopolyCardsDB.activeDeck[25][3] = Integer.toString(25); //Rent with 1 railroad.
		MonopolyCardsDB.activeDeck[25][4] = Integer.toString(50); //Rent with 1 railroad.
		MonopolyCardsDB.activeDeck[25][5] = Integer.toString(100); //Rent with 1 railroad.
		MonopolyCardsDB.activeDeck[25][6] = Integer.toString(200); //Rent with 1 railroad.
		MonopolyCardsDB.activeDeck[25][7] = null; //No houses.
		MonopolyCardsDB.activeDeck[25][8] = null; //No hotels.
		MonopolyCardsDB.activeDeck[25][9] = Integer.toString(100); //Mortgage value.
		MonopolyCardsDB.activeDeck[25][10] = null; //No houses.
		MonopolyCardsDB.activeDeck[25][11] = null; //No hotels.
		MonopolyCardsDB.activeDeck[25][12] = "Cntlt RR."; //Abbreviated name.
		MonopolyCardsDB.activeDeck[25][13] = "   200   "; //Price for display purposes.
		
		MonopolyCardsDB.activeDeck[26][0] = "Cloudsdale"; //Dark blue #1.
		MonopolyCardsDB.activeDeck[26][1] = "Property"; //Property, railroad, or utility.
		MonopolyCardsDB.activeDeck[26][2] = Integer.toString(350); //Price.
		MonopolyCardsDB.activeDeck[26][3] = Integer.toString(35); //Rent.
		MonopolyCardsDB.activeDeck[26][4] = Integer.toString(175); //Rent with 1 house.
		MonopolyCardsDB.activeDeck[26][5] = Integer.toString(500); //Rent with 2 houses.
		MonopolyCardsDB.activeDeck[26][6] = Integer.toString(1100); //Rent with 3 houses.
		MonopolyCardsDB.activeDeck[26][7] = Integer.toString(1300); //Rent with 4 houses.
		MonopolyCardsDB.activeDeck[26][8] = Integer.toString(1500); //Rent with hotel.
		MonopolyCardsDB.activeDeck[26][9] = Integer.toString(175); //Mortgage value.
		MonopolyCardsDB.activeDeck[26][10] = Integer.toString(200); //Price of houses.
		MonopolyCardsDB.activeDeck[26][11] = Integer.toString(200); //Price of hotels.
		MonopolyCardsDB.activeDeck[26][12] = "Cldsdale."; //Abbreviated name.
		MonopolyCardsDB.activeDeck[26][13] = "   350   "; //Price for display purposes.
		
		MonopolyCardsDB.activeDeck[27][0] = "Canterlot"; //Dark blue #2.
		MonopolyCardsDB.activeDeck[27][1] = "Property"; //Property, railroad, or utility.
		MonopolyCardsDB.activeDeck[27][2] = Integer.toString(400); //Price.
		MonopolyCardsDB.activeDeck[27][3] = Integer.toString(50); //Rent.
		MonopolyCardsDB.activeDeck[27][4] = Integer.toString(200); //Rent with 1 house.
		MonopolyCardsDB.activeDeck[27][5] = Integer.toString(600); //Rent with 2 houses.
		MonopolyCardsDB.activeDeck[27][6] = Integer.toString(1400); //Rent with 3 houses.
		MonopolyCardsDB.activeDeck[27][7] = Integer.toString(1700); //Rent with 4 houses.
		MonopolyCardsDB.activeDeck[27][8] = Integer.toString(2000); //Rent with hotel.
		MonopolyCardsDB.activeDeck[27][9] = Integer.toString(200); //Mortgage value.
		MonopolyCardsDB.activeDeck[27][10] = Integer.toString(200); //Price of houses.
		MonopolyCardsDB.activeDeck[27][11] = Integer.toString(200); //Price of hotels.
		MonopolyCardsDB.activeDeck[27][12] = "Canterlot"; //(Un-)abbreviated name.
		MonopolyCardsDB.activeDeck[27][13] = "   400   "; //Price for display purposes.
	}
}
