
public class MonopolyMoneyManager {
	private int pID;
	private int money;
	private String name;
	
	public MonopolyMoneyManager(int playerNum, String playerName, int playerMoney) {
		pID = playerNum;
		name = playerName;
		money = playerMoney;
	}
	
	public int getID() {
		return pID;
	}
	
	public int getMoney() {
		return money;
	}
	
	public String getName() {
		return name;
	}
	
	public int modifyMoney(int change) {
		money += change;
		return money;
	}
}
