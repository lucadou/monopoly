/* Monopoly application
 * A game of Monopoly, to be played with solo (against the computer)
 * or with others.
 *  Copyright (C) 2016 David Lucadou
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.Scanner;

public class MonopolyLaunch { 
	public static Scanner kb = new Scanner(System.in);
	public static MonopolyPlayerManager p1;
	public static MonopolyPlayerManager p2;
	public static MonopolyPlayerManager p3;
	public static MonopolyPlayerManager p4;
	private static String[] pieces = {"♜", "❆", "☢", "☯", "☆", "☘", "♫", "☁"};
	
	public static void main(String[] args) {
		MonopolyCardsDB.populateCardList(); //Initialize card list.
		MonopolyBoardDraw.startColoredPrinter();
		
		
		System.out.println("            .---.\n            |#__|\n           =;===;=\n           / - - \\\n          ( _'.'_ )\n         .-`-'^'-`-.\n        |   `>o<'   |\n        /     :     \\\n       /  /\\  :  /\\  \\\n     .-'-/ / .-. \\ \\-'-.\n      |_/ /-'   '-\\ \\_|\n         /|   |   |\\\n        (_|  /^\\  |_)\n          |  | |  |\n          |  | |  |\n        '==='= ='==='");
		System.out.println("Welcome to Monopoly! The following decks are available for play currently:");
		MonopolyCardsDB.listCards();
		System.out.println();
		
		boolean canCont = false;
		int choice;
		do {
			System.out.print("Type the number of the deck you would like to play with: ");
			choice = kb.nextInt();
			kb.nextLine();
			canCont = MonopolyCardsDB.isDeckChoiceValid(choice);
			if(!canCont) {
				System.out.println("That is not a valid deck.");
			}
		} while(!canCont);
		
		MonopolyCardsDB.generateCurrentDeck(choice);
		
		/*canCont = false;
		int players;
		do{
			System.out.println("How many players will there be? (2-4)");
			players = kb.nextInt();
			kb.nextLine();
			if(players > 1 && players <= 4) //Must be between 2-4 characters.
			{
				canCont = true;
			}
			else
			{
				canCont = false;
				System.out.println("That is not a valid choice.");
			}
		}while(!canCont);*/ //Uncomment when done with everything to implement multiplayer.
		
		//createPlayers(players);
		System.out.println("[Debug] For debug purposes, there is 1 player, name \"Test\", piece ♜.");
		p1 = new MonopolyPlayerManager("Test", "♜", 0); //For testing purposes ONLY! Remove in final builds.
		
		MonopolyBoardDraw.drawBoard();
		
		
	}
	
	private static void createPlayers(int numPlayers) {
		String name;
		String symbol;
		int symbolID;
		if(numPlayers == 2) {
			do {
				System.out.print("Player 1, enter your name: ");
				name = kb.nextLine();
			} while(name.length() <= 0);
			do {
				System.out.println("Choose your piece by entering the number for your piece.");
				listPieces();
				symbolID = kb.nextInt();
				kb.nextLine();
			} while(symbolID < 0 && symbolID >= 8);
			symbol = pieces[symbolID];		
			
			p1 = new MonopolyPlayerManager(name, symbol, 0);
			
			do {
				System.out.print("Player 2, enter your name: ");
				name = kb.nextLine();
			} while(name.length() <= 0);
			do {
				System.out.println("Choose your piece by entering the number for your piece.");
				listPieces();
				symbolID = kb.nextInt();
				kb.nextLine();
			} while(symbolID < 0 && symbolID >= 8);
			symbol = pieces[symbolID];		
			
			p2 = new MonopolyPlayerManager(name, symbol, 0);
		}
		
		if (numPlayers == 3) {
			do {
				System.out.print("Enter your name: ");
				name = kb.nextLine();
			} while(name.length() <= 0);
			do {
				System.out.println("Choose your piece by entering the number for your piece.");
				listPieces();
				symbolID = kb.nextInt();
				kb.nextLine();
			} while(symbolID < 0 && symbolID >= 8);
			symbol = pieces[symbolID];		
			
			p1 = new MonopolyPlayerManager(name, symbol, 0);
			
			do {
				System.out.print("Player 2, enter your name: ");
				name = kb.nextLine();
			} while(name.length() <= 0);
			do {
				System.out.println("Choose your piece by entering the number for your piece.");
				listPieces();
				symbolID = kb.nextInt();
				kb.nextLine();
			} while(symbolID < 0 && symbolID >= 8);
			symbol = pieces[symbolID];		
			
			p2 = new MonopolyPlayerManager(name, symbol, 0);
			
			do {
				System.out.print("Player 3, enter your name: ");
				name = kb.nextLine();
			} while(name.length() <= 0);
			do {
				System.out.println("Choose your piece by entering the number for your piece.");
				listPieces();
				symbolID = kb.nextInt();
				kb.nextLine();
			} while(symbolID < 0 && symbolID >= 8);
			symbol = pieces[symbolID];		
			
			p3 = new MonopolyPlayerManager(name, symbol, 0);
		}
		
		if(numPlayers == 4) {
			do {
				System.out.print("Player 1, enter your name: ");
				name = kb.nextLine();
			} while(name.length() <= 0);
			do {
				System.out.println("Choose your piece by entering the number for your piece.");
				listPieces();
				symbolID = kb.nextInt();
				kb.nextLine();
			} while(symbolID < 0 && symbolID >= 8);
			symbol = pieces[symbolID];		
			
			p1 = new MonopolyPlayerManager(name, symbol, 0);
			
			do {
				System.out.print("Player 2, enter your name: ");
				name = kb.nextLine();
			} while(name.length() <= 0);
			do {
				System.out.println("Choose your piece by entering the number for your piece.");
				listPieces();
				symbolID = kb.nextInt();
				kb.nextLine();
			} while(symbolID < 0 && symbolID >= 8);
			symbol = pieces[symbolID];		
			
			p2 = new MonopolyPlayerManager(name, symbol, 0);
			
			do {
				System.out.print("Player 3, enter your name: ");
				name = kb.nextLine();
			} while(name.length() <= 0);
			do {
				System.out.println("Choose your piece by entering the number for your piece.");
				listPieces();
				symbolID = kb.nextInt();
				kb.nextLine();
			} while(symbolID < 0 && symbolID >= 8);
			symbol = pieces[symbolID];		
			
			p3 = new MonopolyPlayerManager(name, symbol, 0);
			
			do {
				System.out.print("Player 4, enter your name: ");
				name = kb.nextLine();
			} while(name.length() <= 0);
			do {
				System.out.println("Choose your piece by entering the number for your piece.");
				listPieces();
				symbolID = kb.nextInt();
				kb.nextLine();
			} while(symbolID < 0 && symbolID >= 8);
			symbol = pieces[symbolID];		
			
			p4 = new MonopolyPlayerManager(name, symbol, 0);
		}
	}
	
	private static void listPieces() {
		for(int i = 0; i < pieces.length; i++) {
			System.out.println(i + ": " + pieces[i]);
		}
	}
}



/*
 * http://www.retrojunkie.com/asciiart/sports/monopoly.htm
 */

